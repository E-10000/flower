# 介绍
vue有vue-cli脚手架,但uniapp的脚手架我下不下来(主要是npm下不来那脚手架),所以就上gitee随便找了个基本的脚手架,然后经过自己的深度改造

原项目:https://gitee.com/xlzy520/uniapp-tailwind-uview-starter

- 路由使用了`uni-simple-router`,于`src/router/index.js`,使得路由变得和vue的一样,[使用文档](https://hhyang.cn/v2/start/introduction.html)
  值得注意的是，跳转到非tabbar用push，跳转到tabbar，用pushTab
- 封装了vuex,在store下
- 封装了token的消息头请求,如果有记录token,都会请求的.在登陆的时候获得token,调用`utils/setTokenAndUserInfo`,然后在`utils/request`获取`uni.getStorageSync('token')`.接下来在路由`src/router/index.js`中判断,如果有则加入vuex进行响应式使用
- 推荐使用`better-mock`做mock假数据请求,啥都不用封装拦截,很方便(本项目还没安装)[better-mock](https://lavyun.gitee.io/better-mock/document/)
- api都封装好了,写好直接用就行
- 页面的tabbar和注册都要写在`src/pages.json`中

# 使用步骤：
## 1、安装
- 建议先下个HBuildertX,能一步傻瓜式部署
- 命令行执行`npm install `，安装所需依赖

## 2、启动
### h5的方式（推荐，我先做的网页版在做的小程序版本）
```bash
npm run serve
```
### 小程序

先安装微信开发者工具和HbuilderX，2者都打开

1. HbuilderX导入项目


2. 微信开发者工具获取服务端口

![image-20211130150814801](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130150814801.png)

![image-20211130151002576](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151002576.png)

![image-20211010154327555](https://cdn.jsdelivr.net/gh/E-10000/picture/img/237860a918635b269b10ee0894ed08e1.png)

![image-20211130151128595](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151128595.png)

3. 微信开发者工具获取AppId

   你先扫码登录，不要用游客模式登录微信开发者工具

   ![image-20211130151817268](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151817268.png)

   ![image-20211130151837978](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151837978.png)

   ![image-20211130151905339](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151905339.png)

   

   4. 启动

![image-20211130151451367](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130151451367.png)

会弹出来

![image-20211130152216451](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130152216451.png)

![image-20211130153357833](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130153357833.png)

### 码农版启动小程序

上面一样在微信开发者工具获取AppId，然后

```bash
npm run dev:mp-weixin
```

直接导入就行

##  3、预览

![image-20211130153731387](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130153731387.png)

![image-20211130153751823](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130153751823.png)

![image-20211130153853234](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130153853234.png)

![image-20211130153931857](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130153931857.png)

![image-20211130154030125](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130154030125.png)

![image-20211130154043415](https://cdn.jsdelivr.net/gh/E-10000/picture/img/image-20211130154043415.png)

### 项目地址

