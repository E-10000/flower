import _request from '@/utils/request'

const baseUrl = process.env.VUE_APP_BASE_URL
const request = (url, ...arg) => _request(baseUrl + url, ...arg)


export function detect(data) {
  return request('/detect', data)
}

export function saveScoreAndReturnAnswer(data) {
  return request('/saveScoreAndReturnAnswer/' + data, null, 'GET')
}

export function reset(data) {
  return request('/reset' , data, 'GET')
}
