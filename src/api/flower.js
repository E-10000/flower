import _request from '@/utils/request'

const baseUrl = process.env.VUE_APP_BASE_URL
const request = (url, ...arg) => _request(baseUrl + url, ...arg)


export function getWeekFlower(data) {
  return request('/getWeekFlower', data ,'GET')
}

export function spikeList(data) {
  return request('/spikeList', data,'GET')
}

export function getFlower(data) {
  return request('/getFlower' , data, )
}


export function birthday(data) {
  return request('/birthday', data,'GET')
}

export function love(data) {
  return request('/love', data,'GET')
}

export function respect(data) {
  return request('/respect', data,'GET')
}

export function forMyself(data) {
  return request('/forMyself', data,'GET')
}




