import _request from '@/utils/request'

const baseUrl = process.env.VUE_APP_BASE_URL
const request = (url, ...arg) => _request(baseUrl + url, ...arg)



export function getPendingPaymentList(data) {
  return request('/getPendingPaymentList' , data )
}

export function getCompletedList(data) {
  return request('/getCompletedList' , data )
}

export function getOrder(data) {
  return request('/getOrder/' + data ,null )
}





