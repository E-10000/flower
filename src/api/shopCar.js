import _request from '@/utils/request'

const baseUrl = process.env.VUE_APP_BASE_URL
const request = (url, ...arg) => _request(baseUrl + url, ...arg)



export function insertShopCar(data) {
  return request('/addShopCard' , data )
}

export function shopCarPay(data) {
  return request('/shopCarPay' , data )
}

export function shopCar(data) {
  return request('/shopCar' , data, 'GET' )
}

export function deleteShopCar(data) {
  return request('/deleteShopCar/' +  data,null  )
}




