import _request from '@/utils/request'

const baseUrl = process.env.VUE_APP_BASE_URL
const request = (url, ...arg) => _request(baseUrl + url, ...arg)


export function register(data){
  return request('/register', data)
}

export function login(data){
  return request('/login', data)
}

export function getUserInfo(){
  return request('/getUserInfo')
}

export function getAddress(data){
  return request('/getAddress')
}

export function updateAddress(data){
  return request('/updateAddress',data)
}

export function updateUserInfo(data){
  return request('/updateUserInfo',data)
}
