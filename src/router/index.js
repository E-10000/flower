
import {RouterMount,createRouter} from 'uni-simple-router';
import setTokenAndUserInfo from '@/utils/setTokenAndUserInfo'
import store from "@/store";

const router = createRouter({
  platform: process.env.VUE_APP_PLATFORM,
  routes: [...ROUTES]
});
//全局路由前置守卫
router.beforeEach((to, from, next) => {
  const uniUserInfo = uni.getStorageSync('userInfo')
  const uniToken = uni.getStorageSync('token')

  if (uniToken){
    if (uniUserInfo){
      store.dispatch('user/setToken', uniToken)
      store.dispatch('user/setUserInfo', uniUserInfo)
    }else{
      setTokenAndUserInfo(uniToken)
    }
  }
  // console.log(uniToken)
  // console.log(uniUserInfo)
  next();
});
// 全局路由后置守卫
router.afterEach((to, from) => {
  // console.log('跳转结束')
})

export {
  router,
  RouterMount
}
