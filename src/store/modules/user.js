const state = {
  token: null,
  userInfo: null
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_INFO: (state, userInfo) => {
    state.userInfo = userInfo
  }
}

const actions = {
  setToken({commit}, token) {
    commit('SET_TOKEN', token)
  },
  setUserInfo({commit}, userInfo) {
    commit('SET_USER_INFO', userInfo)

  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
