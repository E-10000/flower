import setting from '../setting'

let devUrl = setting.baseUrl

const isDev = process.env.NODE_ENV === 'development'

// #ifdef H5
devUrl = ''
// #endif

const baseUrl = devUrl

const POST = 'POST'
const UPLOAD = 'UPLOAD'
//垃圾俊杰状态码搞成String类型
const SUCCESS_CODES = '200'
const RefreshCode = '506'
const LogoutCode = '503'

const getHeader = () => {
  const token = uni.getStorageSync('token') || ''

  return {
    Authorization: 'Bearer ' + token,
  }
}
/**
 *
 * @param {string} method 请求方法
 * @param {string} url api地址
 * @param {string} data 入参
 */

const request = (url, data, method = POST) => new Promise((resolve, reject) => {
  uni.showLoading({
    title: '加载中'
  });
  //先判断环境,只有h5才做跨域和代理
  let base_url = ''
  if (process.env.UNI_PLATFORM === 'h5'){
    base_url = `${url}`
  }else {
    base_url = process.env.VUE_APP_BASE_IP + `${url}`
  }

  if (method !== UPLOAD) {
    uni.request({
      // url: `${baseUrl}/api${url}`,
      // url: `${url}`,
      url :base_url,
      method,
      data,
      header: getHeader(),
      success(res) {
        // 请求成功
        const data = res.data
        if (data.code === SUCCESS_CODES) {
          uni.hideLoading()
          resolve(data)
        } else {
          // 其他异常、其他的以后再说吧
          console.log(data)
          if (data.code === RefreshCode) {

          } else if (data.code === LogoutCode) {
            uni.hideLoading()
            reject(data)
            return
            // uni.navigateTo({ url: '/pages/login/index' })
          } else {
            uni.showToast({ title: data.message ? '[' + data.code +']' + data.message:'[500]请求错误'  , icon: 'none' })
          }
          uni.showToast({ title: data.message ? '[' + data.code +']' + data.message:'[500]请求错误'  , icon: 'none' })
          uni.hideLoading()
          reject(data)
        }
      },

      fail(err) {
        console.log(err)
        // 请求失败
        uni.hideLoading()
        reject(new Error('请检查网络'))
      },

    })
  } else {
    console.log(data, url)
    uni.uploadFile({
      file: data,
      filePath: data.path,
      name: 'file',
      // url: `${baseUrl}/api/${url}`,
      url :base_url,
      header: getHeader(method, url, true),
      success: resObj => {
        const res = JSON.parse(resObj.data)
        if (res.code === SUCCESS_CODES) {
          uni.hideLoading()
          resolve(res.data)
        } else {

          // 其他异常
          uni.hideLoading()
          reject(res.msg)
        }
      },
      fail: (err) => {
        uni.hideLoading()
        reject(new Error('上传失败:' + JSON.stringify(err)))
      },
    })
  }
})

export default request
