import Vue from 'vue'
import Vuex from "vuex";
import store from "@/store";
import {getUserInfo} from '@/api/user'

export default async function (token) {
  //1、保存在vuex
  store.dispatch('user/setToken', token)
  //2、保存在缓存
  uni.setStorage({
    key: 'token',
    data: token,
  })

  // console.log(store.getters.token)
  // uni.getStorage({
  //   key: 'token',
  //   success(res){
  //     console.log(res.data)
  //   }
  // })
  const res = await getUserInfo()
  const user = res.data
  store.dispatch('user/setUserInfo', user)
  uni.setStorage({
    key: 'userInfo',
    data: user,
  })
  // uni.getStorage({
  //   key: 'userInfo',
  //   success(res){
  //     console.log(res.data)
  //   }
  // })
  return user
}
