//vue.config.js
const TransformPages = require('uni-read-pages')
const {webpack} = new TransformPages()
module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        ROUTES: webpack.DefinePlugin.runtimeValue(() => {
          const tfPages = new TransformPages({
            includes: ['path', 'name', 'aliasPath']
          });
          return JSON.stringify(tfPages.routes)
        }, true)
      })
    ]
  },
  //这里的设置仅适用于h5，小程序端不用到这里的
  devServer: { // 包含关系
    proxy: {
      '/flower': {
        // target: 'http://www.xingxingyihuo.cn:8081',
        target: process.env.VUE_APP_BASE_IP,
        changeOrigin: true
      }
    }
  },
}
